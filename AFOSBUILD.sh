rm -rf /opt/ANDRAX/urlcrazy/

bundle install

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Bundle install... PASS!"
else
  # houston we have a problem
  exit 1
fi

cp -Rf $(pwd) /opt/ANDRAX/urlcrazy/

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
